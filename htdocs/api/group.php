<?php

/**
 * listGroups
 *
 * gets all groups. Include list of members if requested.
 *
 * @param boolean $addMembers
 * @param string $group_id
 * @return array
 * @author Robin
 */

function listGroups($addMembers = false,$group_id){
	$where = ($group_id == -1) ? "" : ' where groups.group_id = '.intval($group_id)." ";
	$groups = $GLOBALS['DB']->query("SELECT groups.name , groups.institution , groups.group_id, users.firstname as leader_firstname, users.lastname as leader_lastname, groups.leader_id, count(distinct members.user_id) as nbUsers from groups left join users on groups.leader_id = users.user_id left join user_groups on groups.group_id = user_groups.group_id left join users as members on user_groups.user_id = members.user_id and members.is_active = 'Y' $where group by user_groups.group_id order by groups.name")->fetchAll();
		foreach($groups as $idx => $group){
			if($addMembers){
				$groups[$idx]['members'] = $GLOBALS['DB']->select(
					"users",
					array(
						"[><]user_groups" => "user_id"
					),
					array(
						"firstname",
						"lastname",
						"users.user_id (user_id)"
					),
					array(
						'AND' => array(
							'users.is_active' => "Y",
							'user_groups.group_id' => $group['group_id']
						)
					)
				);
			}


			$groups[$idx]['projects'] = $GLOBALS['DB']->select(
				"projects",
				array("[><]project_groups" => "project_id"),
				array("projects.project_id (project_id)", "projects.name (name)"),
				array("project_groups.group_id" => $group['group_id'])
			);
		}

	return ($group_id == -1) ? $groups : $groups[0];
}

/**
 * createGroup
 *
 * only for *admin*.
 *
 * @param array $group
 * @param stdClass $loggedUser
 * @return int
 * @author Robin
 */

function createGroup($group,$loggedUser){
	$is_admin = check_admin($loggedUser);
	if(!$is_admin) throw new Exception("Permission denied", 501);
	if(!$group['name']) throw new Exception('Name is required', 501);
	if(!$group['institution']) throw new Exception('Institution is required', 501);
	$group['leader_id'] = $GLOBALS['DB']->get("users","user_id",array('AND' => array("is_active" => "Y", "user_id" => $group['leader_id'])));
	if(!$group['leader_id']) throw new Exception('Leader is required', 501);
	if($group['group_id']){
		$GLOBALS['DB']->update('groups',array('name' => $group['name'],'institution' => $group['institution'],'leader_id' => $group['leader_id']),array('group_id' => $group['group_id']));
	}
	else{
		$GLOBALS['DB']->insert('groups',array('name' => $group['name'],'institution' => $group['institution'],'leader_id' => $group['leader_id']));
		$group['group_id'] = $GLOBALS['DB']->id();
	}
	$GLOBALS['DB']->delete('project_groups',array("group_id" => $group['group_id']));
	foreach($group['projects'] as $project){
		$GLOBALS['DB']->insert('project_groups',array("group_id" => $group['group_id'],'project_id' => $project['project_id']));
	}
	if(!$group['group_id']) throw new Exception("ERROR: no group id received", 501);
	$test = $GLOBALS['DB']->count("user_groups",array("AND" => array("user_id" => $group['leader_id'], "group_id" => $group['group_id'])));
	if(!$test) $GLOBALS['DB']->insert('user_groups',array('user_id' => $group['leader_id'],'group_id' => $group['group_id']));
	return intval($group['group_id']);
}

/**
 * deleteGroup
 *
 * delete a group. Only for *admin*
 *
 * @param int $group_id
 * @param stdClass $loggedUser
 * @return boolean
 * @author Robin
 */

function deleteGroup($group_id,$loggedUser){
	$is_admin = check_admin($loggedUser);
	if(!$is_admin) throw new Exception("Permission denied", 501);
	return $GLOBALS['DB']->delete('groups',array('group_id' => $group_id));
}

?>