<?php

use ReallySimpleJWT\TokenBuilder;


function check_password_syntax($password){
	if(strlen($password) < 8) throw new Exception("ERROR: the length of the password should be at least 8 characters", 501);
	if(!preg_match("/[0-9]/",$password)) throw new Exception("ERROR: the password must contain at least one numerical character", 501);
	if(!preg_match("/[a-z]/",$password)) throw new Exception("ERROR: the password must contain at least one lowercase character", 501);
	if(!preg_match("/[A-Z]/",$password)) throw new Exception("ERROR: the password must contain at least one uppercase character", 501);
	return true;
}


function createJWT($user){

	$expiration = time() + 24 * 60 * 60;
	$issuer = 'https://www.vital-it.ch';
	$builder = new TokenBuilder();
	$projects = array_map(function($p){return $p['project_id'];},$user['projects']);
	return $builder->addPayload(['key' => 'firstname', 'value' => $user['firstname']])
		->addPayload(['key' => 'lastname', 'value' => $user['lastname']])
		->addPayload(['key' => 'login', 'value' => $user['login']])
		->addPayload(['key' => 'user_id', 'value' => $user['user_id']])
		->addPayload(['key' => 'projects', 'value' => implode(";",$projects)])
		->addPayload(['key' => 'permissions', 'value' => implode(";",$user['permissions'])])
	    ->setSecret(JWT_SECRET)
	    ->setExpiration($expiration)
	    ->setIssuer($issuer)
	    ->build();
}

function getPreferences($user_id){
	$prefs = $GLOBALS['DB']->select("user_prefs",array(
		"[><]cv_preferences" => "preference_key"
	),
	array("user_prefs.preference_key", "user_prefs.value", "cv_preferences.label" ),
	array("user_prefs.user_id" => $user_id)
	);
	
	if (!$prefs) $prefs = array();
	$return = array();
	foreach($prefs as $pref){
		$return[$pref['preference_key']] = array("value" => $pref['value'],"label" => $pref['label']);
	}
	return $return;
}


/**
 * process
 *
 * gets all details about a user (list of groups and projects)
 *
 * @param array $user
 * @param string $is_admin
 * @return array
 * @author Robin
 */

function process($user,$is_admin = FALSE,$is_leader=FALSE){

	if(!$user) throw new Exception('Unknown user',501);
	else {
		$user['permissions'] = array();
		if(isset($user['is_admin']) && $user['is_admin'] == 'Y') $user['permissions'][] = 'admin';
		$user['groups'] = $GLOBALS['DB']->select(
			"groups",
			array(
				"[><]user_groups" => array("groups.group_id" => "group_id"),
				"[><]users" => array("leader_id" => "user_id")
			),
			array(
				"groups.group_id (group_id)",
				"name",
				"firstname",
				"lastname",
				"groups.leader_id (leader_id)"
			),
			array(
				"user_groups.user_id" => $user['user_id']
			)
		);
		$projects = $GLOBALS['DB']->select(
			"projects",
			array(
				"[><]project_groups" => array("projects.project_id" => "project_id"),
				"[><]user_groups" => array('project_groups.group_id' => 'group_id'),
				"[><]users" => array("user_groups.user_id" => "user_id")
			),
			array(
				"projects.project_id (project_id)",
				"projects.name (name)",
				"projects.description (description)"
			),
			array("user_groups.user_id" => $user['user_id'])
		);
		if($projects && count($projects)){
			foreach($projects as $project){
				$user['projects'][$project['project_id']] = $project;
			}
			$user['projects'] = array_values($user['projects']);
		}
		else $user['projects'] = array();

		$user['is_group_leader'] = 'N';
		if(count($user['groups'])){
			foreach($user['groups'] as $group){
				$user['permissions'][] = 'group_'.$group['group_id'];
				if($group['leader_id'] == $user['user_id']){
					$user['is_group_leader'] = 'Y';
					$user['permissions'][] = 'leader_'.$group['group_id'];
				}
			}
		}
		if(!$is_admin) unset($user['is_admin']);
		$user['code'] = base64_encode($user['login'].":".$user['code']);
		if($user['is_active'] == 'N'){
			$leaders = $GLOBALS['DB']->select(
				"groups",
				array("[><]users" => array("leader_id" => "user_id")),
				array("users.user_id (user_id)", "users.email (email)", "users.firstname (firstname)", "users.lastname (lastname)", "users.login (login)"),
				array("AND" => array("groups.group_id" => $user['groups'][0]['group_id'], "users.is_active" => 'Y'))
			);
			if($leaders && count($leaders)) $user['leader'] = $leaders[0];
		}
		else{
			$user['permissions'][] = 'active';
			if(!$is_admin && !$is_leader) unset($user['activation_code']);
		}
		if(isset($user['password'])) $user['password2'] = $user['password'];
	}
	$user['jwt'] = createJWT($user);
	$user['prefs'] = getPreferences($user['user_id']);
	return $user;
}

/**
 * listUsers
 *
 * list all registered and valid users with their projects and groups membership.
 *
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function listUsers($loggedUser){
	$is_admin = check_admin($loggedUser);
	$loggedUserId = $loggedUser->user_id;
	$leaderGroups = $GLOBALS['DB']->select(
		"groups",
		array(
			"[><]users" => array("leader_id" => "user_id")
		),
		"groups.group_id (group_id)",
		array("user_id" => $loggedUser->user_id	)
	);
	$fields = array(
		'users.user_id' => 'user_id',
		'users.firstname' => 'firstname',
		'users.lastname' => 'lastname',
		'users.phone' => 'phone',
		'users.email' => 'email',
		"group_concat(groups.group_id)" => 'group_ids',
		"group_concat(groups.leader_id)" => 'group_leaders'
	);

	if(DBTYPE == 'sqlite'){
		$fields["group_concat(groups.institution || ': ' || groups.name || ' (' || leader.firstname || ' ' || leader.lastname || ')')"] = 'groups';
	}
	else{
		$fields["group_concat(concat(groups.institution, ': ',groups.name, ' (',leader.firstname,' ',leader.lastname,')') separator ', ')"] = 'groups';
	}

	$where = " where users.is_active = 'Y' ";
	if($is_admin){
		$fields['users.is_active'] = 'active';
		$fields['users.is_admin'] = 'admin';
		$where = '';
	}
	$query = "SELECT ";
	foreach($fields as $sqlfield => $field){
		$query .= $sqlfield." as ".$field.", ";
	}
	$query = rtrim($query,', ');
	$query .= " from users inner join user_groups on users.user_id = user_groups.user_id inner join groups on user_groups.group_id = groups.group_id inner join users as leader on groups.leader_id = leader.user_id ";
	$query .= $where;
	$query .= " group by users.user_id";
	$users = $GLOBALS['DB']->query($query)->fetchAll();

	foreach($users as $idx => $user){
		if($is_admin || count($leaderGroups) || $loggedUserId == $user['user_id']){
			$userGroups = explode(",",$user['group_ids']);
			$users[$idx]['canEdit'] = ($loggedUserId == $user['user_id'] || $is_admin || count(array_intersect($leaderGroups,$userGroups))) ? "Y" : "N";
		}
		else $users[$idx]['canEdit'] = 'N';
		unset($users[$idx]['group_ids']);
		$group_leaders = explode(",",$user['group_leaders']);
		$users[$idx]['is_group_leader'] = (in_array($user['user_id'],$group_leaders)) ? "Y" : "N";
		unset($users[$idx]['group_leaders']);
	}

	return $users;
}

/**
 * login
 *
 * Checks login. Update reset password if activation_code is used as password.
 *
 * @param array $request
 * @return array
 * @author Robin
 */

function login($request){

	$users = $GLOBALS['DB']->select(
	"users",
	array("user_id", "firstname", "lastname", "login", "email", "is_active", "is_admin", "code", "is_password_reset", "password","activation_code", 'ga_secret'),
	array("login" => $request['username']));
	foreach($users as $user){
		if(password_verify($request['password'],$user['password'])){
			if($user['is_active'] == 'D') throw new Exception("ERROR: account has been rejected", 501);
			if($user['is_active'] == 'N') throw new Exception("ERROR: the account request has NOT yet been reviewed.", 501);
			
			// For successfull login with Google Authenticator. We don't allow anymore to request a new QR code
			if($user['is_active'] == 'P' && GA_ENABLED) {
				$GLOBALS['DB']->update('users',array('is_active' => 'Y'),array('user_id' => $user['user_id']));
				$user['is_active'] = 'Y';
			}
			if($user['is_password_reset'] == 'Y'){
				$GLOBALS['DB']->update('users',array('is_password_reset' => 'N'),array('user_id' => $user['user_id']));
				$user['is_password_reset'] = 'N';
			}
			break;
		}
		elseif($user['activation_code'] == $request['password'] && $user['is_password_reset'] == 'Y'){
			break;
		}
		else unset($user);
	}
	if(!isset($user) || !$user) throw new Exception("ERROR: invalid username / password", 501);
	unset($user['password']);
	if(GA_ENABLED){
		if($user['ga_secret']){
			$user['qr_img_src'] = '';
			$user['ga_secret'] = 'active';
		}
		else{
			$user['ga_secret'] = 'new';
		    $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
		    $secret = $secretFactory->create(SITE_TITLE, $user['login']);
		    $secretKey = $secret->getSecretKey();
			$GLOBALS['DB']->update('users',array('ga_secret' => $secretKey,'is_active' => 'P'),array('user_id' => $user['user_id']));
			$qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
		    $user['qr_img_src'] = $qrImageGenerator->generateUri($secret);
		}
		return array('login' => $user['login'],'user_id' => $user['user_id'], 'ga_status' => $user['ga_secret'], 'qr_img_src' => $user['qr_img_src'],'code' => $user['activation_code']);
	}
	return process($user);
}

function ga_login($request){
	$code = $request['ga_code'];
	$user = $GLOBALS['DB']->get(
		"users",
		array("user_id", "firstname", "lastname", "login", "email", "is_active", "is_admin", "code", "is_password_reset","activation_code", 'ga_secret'),
		array("user_id" => $request['user_id'])
	);
	if(!$user) throw new Exception("ERROR: unknown user", 501);

	$googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();

	if ($googleAuthenticator->authenticate($user['ga_secret'], $code)){
		$GLOBALS['DB']->update('users',array('is_active' => 'Y'),array('user_id' => $request['user_id']));
		$user['is_active'] = 'Y';
		return process($user);
	}
	else {
		throw new Exception("ERROR: wrong code provided", 501);
	}
}


function newQR($request){
	$user = $GLOBALS['DB']->get(
		"users",
		array("user_id", "login"),
		array("user_id" => $request['user_id'], 'activation_code' => $user['code'], 'is_active' => 'P')
	);
	if(!$user) throw new Exception("ERROR: not a valid request", 501);
    $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
    $secret = $secretFactory->create(SITE_TITLE, $user['login']);
    $secretKey = $secret->getSecretKey();
	$GLOBALS['DB']->update('users',array('ga_secret' => $secretKey),array('user_id' => $user['user_id']));
	$qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
    return $qrImageGenerator->generateUri($secret);

}
/**
 * getUser
 *
 * gets the user. $user_id could be either int (user_id) or authdata (base64_encode(login:code));
 *
 * @param string $user_id
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function getUser($user_id, $loggedUser){
	$is_admin = $GLOBALS['DB']->get("users","user_id",array("AND" => array("is_active" => "Y", "is_admin" => "Y", "user_id" => $loggedUser->user_id)));

	if(is_numeric($user_id)){
		$user = $GLOBALS['DB']->get("users",array("user_id", "firstname", "lastname", "login", "email", "phone", "is_active", "is_admin", "code", "activation_code", "is_password_reset", "password", 'newsletter','ga_secret'),array("user_id" => $user_id));
	}
	else{
		list($login,$code) = explode(":",base64_decode($user_id));
		$user = $GLOBALS['DB']->get("users",array("user_id", "firstname", "lastname", "login", "email", "phone", "is_active", "is_admin", "code", "is_password_reset", "password", 'newsletter','ga_secret'),array("AND" => array("login" => $login,"code" => $code)));
	}

	$user['groups'] = $GLOBALS['DB']->select(
	"groups",
	array("[><]user_groups" => "group_id"),
	array("groups.group_id", "groups.name", "groups.leader_id"),
	array("user_groups.user_id" => $user['user_id'])
	);
	$user['projects'] = $GLOBALS['DB']->select(
		"projects",
		array(
			"[><]project_groups" => "project_id",
			"[><]user_groups" => array("project_groups.group_id" => "group_id")
		),
		array(
			"projects.project_id (project_id)",
			"projects.project_name (project_name)"
		),
		array(
			"user_groups.user_id" => $user['user_id']
		)
	);

	$admin_leader_groups = ($is_admin) ? $GLOBALS['DB']->select('groups','group_id',array("leader_id" => $is_admin)) : array();
	$dbUserGroups = $GLOBALS['DB']->select('user_groups','group_id',array("user_id" => $user['user_id']));
	$is_leader = count(array_intersect($admin_leader_groups,$dbUserGroups));

	if(GA_ENABLED){
	    $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
	    $secret = $secretFactory->create(SITE_TITLE, $user['login']);
		$qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
	    $user['qr_img_src'] = $qrImageGenerator->generateUri($secret);
	}

	return process($user,$is_admin,$is_leader>0);
}

/**
 * register
 *
 * register a new account. The account is not active. An email is sent to the leader of the group and validate the account.
 *
 * @param stdClass $user
 * @return stdClass
 * @author Robin
 */

function register($user){
	$user_id = $GLOBALS['DB']->get("users","user_id",array("email" => $user['email']));
	if($user_id){
		throw new Exception("Email $user[email] is already registered", 501);
	}
	else{
		$required = array('login','firstname','lastname','password','email', 'group_id');

		foreach($required as $require){
			if(!isset($user[$require]) || empty($user[$require])) throw new Exception("$require is not valid", 501);
			$user[$require] = utf8_encode($user[$require]);
		}
		if($user['password'] != $user['password2']) throw new Exception("Password was not confirmed correctly", 501);
		check_password_syntax($user['password']);

		unset($user['password2']);

		$group_id = $GLOBALS['DB']->get('groups',"group_id",array("group_id" => $user['group_id']));
		if(!$group_id) throw new Exception("Group membership is not valid", 501);

		unset($user['group_id']);

		$user['password'] = password_hash($user['password'],PASSWORD_DEFAULT);
		$user['code'] = rand_str(25);
		$user['activation_code'] = rand_str(25);
		$user['is_active'] = 'N';
		$user['is_admin'] = 'N';
		$user['newsletter'] = 'N';
		$db_columns = array_keys($GLOBALS['DB']->get("users","*"));
		$user = (array)$user;
		foreach($user as $k => $v){
			if(!in_array($k,$db_columns)) unset($user[$k]);
		}
		$GLOBALS['DB']->insert("users",$user);
		$user['user_id'] = $GLOBALS['DB']->id();

		$GLOBALS['DB']->insert('user_groups',array('user_id' => $user['user_id'], 'group_id' => $group_id));
		$leaders = $GLOBALS['DB']->select(
			"groups",
			array("[><]users" => array("leader_id" => "user_id")),
			array("users.user_id (user_id)", "users.email (email)", "users.firstname (firstname)", "users.lastname (lastname)", "users.code (code)", "users.login (login)", "groups.name (name)", "users.is_admin (is_admin)", "users.is_active (is_active)"),
			array("AND" => array("groups.group_id" => $group_id, "users.is_active" => 'Y'))
		);

		if(!$leaders || !count($leaders)) throw new Exception("Error This group has no leader", 501);

		$leader = $leaders[0];
		$leader['projects'] = array();

		$user['permissions'] = array("group_".$group_id);
		if($leader){
			$projects = $GLOBALS['DB']->select(
				"projects",
				array(
					"[><]project_groups" => array("projects.project_id" => "project_id"),
					"[><]user_groups" => array('project_groups.group_id' => 'group_id'),
					"[><]users" => array("user_groups.user_id" => "user_id")
				),
				array(
					"projects.project_id (project_id)",
					"projects.name (name)",
					"projects.description (description)"
				),
				array("user_groups.user_id" => $leader['user_id'])
			);
			if($projects && count($projects)){
				foreach($projects as $project){
					$leader['projects'][$project['project_id']] = $project;
				}
				$leader['projects'] = array_values($leader['projects']);
			}
			else $leader['projects'] = array();
			
			$GLOBALS['DB']->insert('user_prefs', array(
				"user_id" => $user['user_id'],
				"preference_key" => "project_id",
				"value" => $projects[0]['project_id']
			));
			
			
			$leader['permissions'] = array('is_active','is_leader','is_admin');
			$leader['jwt'] = createJWT($leader);
			$param = base64_encode($user['user_id'].':'.$user['activation_code'].":".$leader['login'].":".$leader['jwt']);
			$title = "[".SITE_TITLE."] account validation for ".$user['firstname']." ".$user['lastname'];
			$body = "Dear ".$leader['firstname'].", \r\n\r\n";
			$body .= $user['firstname']." ".$user['lastname']." has requested an account for the ".SITE_TITLE." platform as part of your group ".$leader['name'].".\r\n\r\n";
			$body .= "To validate the account, please click on the following link: http://".$_SERVER['SERVER_NAME'].'/#/activation/'.$param."\r\n\r\n";
			$body .= "To reject the account, please click on the following link: http://".$_SERVER['SERVER_NAME'].'/#/reject/'.$param."\r\n\r\n";
			$body .= "Thank you for your cooperation.\r\n\r\n";
			$body .= "========\r\n";
			foreach($required as $require){
				if($require == 'password' || $require == 'group_id') continue;
				$body .= $require.": ".$user[$require]."
";
			}


			$email = new PHPMailer();
			$email->From      = CONTACT_EMAIL;
			$email->FromName  = 'VIKM Team at SIB';
			$email->Subject   = $title;
			$email->Body      = $body;
			$email->AddAddress( $leader['email'] );
			$email->Send();
		}
	}
	return process((array)$user);
}


/**
 * updateUser
 *
 * check permission to edit the user. Update the user details.
 * also used to validate or reject an account, from the links provided in the email to the group leader.
 *
 * @param stdClass $user
 * @param string $adminLogin
 * @param string $adminCode
 * @return array
 * @author Robin
 */

function updateUser($user,$loggedLeader){
	$admin_id = $loggedLeader->user_id;
	$admin_is_admin = (in_array('is_admin',explode(";",$loggedLeader->permissions)));
	$admin_leader_groups = ($admin_id) ? $GLOBALS['DB']->select('groups','group_id',array("leader_id" => $admin_id)) : array();
	$dbUser = $GLOBALS['DB']->get("users",array("user_id","login","firstname","lastname","password","phone","email","is_admin","is_active","code","activation_code","is_password_reset","newsletter"),array("user_id" => $user['user_id']));
	$dbUser['groups'] = $GLOBALS['DB']->select('user_groups','group_id',array("user_id" => $user['user_id']));
	$is_leader = count(array_intersect($admin_leader_groups,$dbUser['groups']));
	if(!$dbUser) throw new Exception("Unknown user",501);

	if(!$user['groups'] || !count($user['groups'])) throw new Exception("User is not part of any group !", 501);

	$GLOBALS['DB']->delete('user_groups',array("user_id" => $user['user_id']));
	foreach($user['groups'] as $group){
		$GLOBALS['DB']->insert('user_groups',array('user_id' => $user['user_id'], 'group_id' => $group['group_id']));
	}

	if(!isset($user['password'])) $user['password'] = $dbUser['password'];
	if(!isset($user['password2'])) $user['password2'] = $dbUser['password'];
	if($user['password'] != $user['password2']) throw new Exception("Password was not confirmed correctly", 501);

	unset($user['password2']);

	// activate account
	if($dbUser['is_active'] == 'N' && $user['is_active'] == 'Y'){ // we set active

		if(!check_leader($dbUser['groups'][0],$admin_id) && !$admin_is_admin) throw new Exception("Permission denied. Leader is not valid", 501);

		$GLOBALS['DB']->update('users',array("is_active" => 'Y'),array("user_id" => $user['user_id']));

		// send email //
		$title = "[".SITE_TITLE."] account validated";
		$body = "Dear ".$dbUser['firstname'].", \r\n\r\n";
		$body .= "Your account for the ".SITE_TITLE." platform has been activated.\r\n\r\n";
		$body .= "You can login at http://".$_SERVER['SERVER_NAME']."\r\n\r\n";
		$headers =    "MIME-Version: 1.0\r\n" .
		               "Content-type: text/plain; charset=utf-8; format=flowed\r\n" .
		               "Content-Transfer-Encoding: 8bit\r\n" .
					   "From: ".CONTACT_EMAIL ."\r\n" .
		               "X-Mailer: PHP" . phpversion();

		mail($dbUser['email'],$title,$body,$headers);
		return true;
	}
	// reject account
	elseif($user['is_active'] == 'R'){
		if(!check_leader($dbUser['groups'][0],$admin_id) && !$admin_is_admin) throw new Exception("Permission denied. Leader is not valid", 501);
		$leader = $GLOBALS['DB']->get('users',array('firstname','lastname','email'),array('user_id' => $admin_id));
		$GLOBALS['DB']->update('users',array("is_active" => 'R'),array("user_id" => $user['user_id']));

		// send email //
		$title = "[".SITE_TITLE."] account rejected";
		$body = "Dear ".$dbUser['firstname'].", \r\n\r\n";
		$body .= "Your account for the ".SITE_TITLE." platform has been rejected.\r\n\r\n";
		$body .= "Please contact ".$leader['firstname']." ".$leader['lastname']." at ".$leader['email']." for further explanation.\r\n\r\n";
		$headers =    "MIME-Version: 1.0\r\n" .
		               "Content-type: text/plain; charset=utf-8; format=flowed\r\n" .
		               "Content-Transfer-Encoding: 8bit\r\n" .
					   "From: ".CONTACT_EMAIL ."\r\n" .
					   "Cc: ".$leader['email']."\r\n" .
		               "X-Mailer: PHP" . phpversion();

		mail($dbUser['email'],$title,$body,$headers);
		return true;
	}

	else{
		$is_user = ($loggedLeader->login == $dbUser['login']);
		if(!$admin_is_admin && !$is_user && !$is_leader) throw new Exception("Permission denied", 501);
		$fields = array('firstname','lastname','password','email', 'phone','newsletter');
		if($admin_is_admin){
			$fields[] = 'is_admin';
			$fields[] = 'is_active';
		}
		$updates = array();
		foreach($fields as $field){
			if((!isset($user[$field]) || empty($user[$field])) && $field != 'phone') throw new Exception("$field is not valid", 501);
			if($admin_is_admin){
				if(!in_array($user['is_admin'],array('Y','N'))) throw new Exception("Is Admin is not valid", 501);
				if(!in_array($user['is_active'], array('Y','N'))) throw new Exception("Is Active is not valid", 501);
				if(!in_array($user['newsletter'], array('Y','N'))) throw new Exception("Newsletter is not valid", 501);
			}
			if($field == 'password' && $dbUser['password'] != $user['password']){
				check_password_syntax($user['password']);
				$user['password'] = password_hash($user['password'],PASSWORD_DEFAULT);
				$updates['is_password_reset'] = 'N';
			}
			$updates[$field] = $user[$field];
		}
		$GLOBALS['DB']->update('users',$updates,array('user_id' => $user['user_id']));
		$user = process((array)$user,$admin_is_admin);
		return $user;
	}

}

/**
 * resetPassword
 *
 * resetPassword. Set _is_password_reset_ to 'Y' and send an email with the login and activation_code as temporary password.
 *
 * @param string $email
 * @return boolean
 * @author Robin
 */


function resetPassword($email){
	
	$new_code = rand_str(25);
	
	$user = $GLOBALS['DB']->get('users',array('user_id','firstname','login','activation_code','email'),array('email' => $email));
	if(!$user) throw new Exception("No account with this email address.",501);
	// reset password //

	$GLOBALS['DB']->update('users',array('is_password_reset' => 'Y','activation_code' => $new_code),array("user_id" => $user['user_id']));
	$title = "[".SITE_TITLE."] password reset";
	$body = "Dear ".$user['firstname'].", \r\n\r\n";
	$body .= "A temporary password has been created to access the ".SITE_TITLE." platform.\r\n\r\n";
	$body .= "login: ".$user['login']."\r\n";
	$body .= "password: ".$new_code."\r\n\r\n";
	$body .= "You can login at http://".$_SERVER['SERVER_NAME']."\r\n\r\n";
	$headers =    "MIME-Version: 1.0\r\n" .
           "Content-type: text/plain; charset=utf-8; format=flowed\r\n" .
           "Content-Transfer-Encoding: 8bit\r\n" .
				   "From: ".CONTACT_EMAIL ."\r\n" .
           "X-Mailer: PHP" . phpversion();
	mail($user['email'],$title,$body,$headers);
	return true;
}


function updateUserPrefs($prefs,$user){
	$pref_key = $GLOBALS['DB']->get("cv_preferences","preference_key",array("preference_key" => $prefs['preference_key']));
	if (!$pref_key) throw new Exception("Error: preference key is unknown",	501);
	$preference_id = $GLOBALS['DB']->get('user_prefs',"user_pref_id",array("AND" => array("user_id" => $user->user_id, "preference_key" => $pref_key)));
	if ($preference_id) $GLOBALS['DB']->update("user_prefs",array("value" => $prefs['value']), array("user_pref_id" => $preference_id));
	else $GLOBALS['DB']->insert("user_prefs",array(
		"user_id" => $user->user_id,
		"preference_key" => $pref_key,
		"value" => $prefs['value']
	));
	return $preference_id;
}


?>
