<?php

/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require '../vendor/autoload.php';
require '../../tools/include.php';
use ReallySimpleJWT\Token;


// this should activate the debug mode
$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
    ]
]);

/**
 * authenticate
 *
 * route middleware for authentication. Gets user login and code from Authorization basic header.
 *
 * @param Route $route
 * @return boolean
 * @author Robin
 */


$jwtauth = function ($request, $response, $next) {
   $token_array = $request->getHeader('HTTP_AUTHORIZATION');

   if (count($token_array) == 0) {
      $data = Array(
	 "jwt_status" => "token_not_exist"
      );

      return $response->withJson($data, 401)
		      ->withHeader('Content-type', 'application/json');
   }

   $token = $token_array[0];
   try
   {
      // $tokenDecoded = JWT::decode($token, JWT_SECRET, array('HS256'));
      $user = json_decode(Token::getPayload($token));
	  if(strpos($user->permissions,'active') === FALSE){
		$code = $GLOBALS['DB']->get("users",'code',array("user_id" => $user->user_id));
		  $login = $user->login;
		  if(urldecode($request->getUri()->getPath()) !== 'user/'.base64_encode($login.":".$code)) throw new Exception('The account is not active',501);
	  }
	  $request = $request->withAttribute('user', $user);
      $response = $next($request, $response);
      return $response;
   }
   catch(Exception $e)
   {
	   // error_log($e->getMessage());
      $data = Array(
	 "jwt_status" => "token_invalid"
      );

      return $response->withJson($data, 401)
		      ->withHeader('Content-type', 'application/json');
   }
};



/**
 * check_leader
 *
 * check if a user is a leader of a given group.
 *
 * @param int $group_id
 * @param int $leaderID
 * @return int
 * @author Robin
 */
function check_leader($group_id, $leaderID){
	$groups = $GLOBALS['DB']->select("groups",array('[><]users' => array("leader_id" => "user_id")),array("group_id"),array('AND' => array("users.is_active" => "Y", "users.user_id" => $leaderID,"groups.group_id" => $group_id)));
	$group_id = ($groups && count($groups)) ? $groups[0]['group_id'] : 0;
	return $group_id;
}

/**
 * check_admin
 *
 * provide a user either with a _user_id_ or a _login_ and _code_. Checks if _is_admin_ == 'Y'
 *
 * @param int $ID
 * @param string $login
 * @param string $code
 * @return void
 * @author Robin
 */

function check_admin($user){
	return in_array('admin',explode(";",$user->permissions));
}


/**
 * check_project
 *
 * check if a user can access a project.
 *
 * @param string $project_id
 * @param string $user_id
 * @return int
 * @author Robin
 */

function check_project($project_id,$user_id){
	$projects = $GLOBALS['DB']->select("project_groups",array(
		"[><]user_groups" => "group_id",
		"[><]users" => array("user_groups.user_id" => "user_id")
	),
	"project_groups.project_id",
	array(
		"AND" => array(
			"users.user_id" => $user_id,
			"project_groups.project_id" => $project_id
		)
	)
	);
	return (count($projects)) ? $projects[0] : null;
}

function db_log($query){
	error_log(preg_replace("/\s+/"," ",$query['query']));
}


$c = $app->getContainer();
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
		$statusCode = ($exception->getCode() !== 501) ? 500 : 501;
        return $c['response']->withStatus($statusCode)
                             ->withHeader('Content-Type', 'application/json')
                             ->write(json_encode($exception->getMessage()));
    };
};

$app->get('/user',function ($request,$response) {
	require 'user.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$users = listUsers($loggedUser);
	return 	$response = $response->withJson($users);

})->add($jwtauth);

$app->get('/user/{user_id}', function ($request,$response,$args) {
	require 'user.php';
	$user_id = $args['user_id'];
	$headers = $request->getHeaders();
	$loggedLeader = $request->getAttribute('user');
	$user = getUser($user_id,$loggedLeader);
	return 	$response = $response->withJson($user);

})->add($jwtauth);

$app->post('/authenticate', function ($request,$response) {
	require 'user.php';
	$user = $request->getParsedBody();
	$login = login($user);
	return 	$response = $response->withJson($login);
});
$app->post('/ga_authenticate', function ($request,$response) {
	require 'user.php';
	$user = $request->getParsedBody();
	$login = ga_login($user);
	return 	$response = $response->withJson($login);
});
$app->post('/new_qr', function ($request, $response){
	require 'user.php';
	$request = $request->getParsedBody();
	$qr = newQR($request);
	return $qr;
});

$app->post('/user', function ($request,$response) {
	require 'user.php';
	$request = $request->getParsedBody();
	$user = register($request);
	return $response->withJson($user);
});


$app->put('/user', function ($request,$response) {
	require 'user.php';
	$headers = $request->getHeaders();
	$loggedLeader = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$user = updateUser($request,$loggedLeader);
	return $response->withJson($user);
})->add($jwtauth);


$app->get('/group[/{:group_id}]', function ($request,$response) {
	require 'group.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$group_id = $request->getQueryParam('group_id',$default=-1);
	$addMembers = $request->getQueryParam('members');
	$groups = listGroups($addMembers,$group_id);
	return $response->withJson($groups);
});


$app->post('/group', function ($request,$response) {
	require 'group.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$groups = createGroup($request,$loggedUser);
	return $response->withJson($groups);


})->add($jwtauth);


$app->put('/group', function ($request,$response) {
	require 'group.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$groups = createGroup($request,$loggedUser);
	return  $response->withJson($groups);
})->add($jwtauth);

$app->delete('/group/{group_id}', function ($request,$response ,$args) {
	require 'group.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$group_id = $args['group_id'];
	$groups = deleteGroup($group_id,$loggedUser);
	return $response->withJson($groups);
})->add($jwtauth);

$app->post('/resetpass', function ($request,$response) {
	require 'user.php';
	$request = $request->getParsedBody();
	echo resetPassword($request['email']);

});

$app->get('/project/{project_id}/news', function ($request,$response,$args) {
	require 'news.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$project_id = $args['project_id'];
	$news = listNews($project_id,$loggedUser);
	return $response->withJson($news);

})->add($jwtauth);



$app->get('/news/{news_id}', function ($request,$response,$args) {
	require 'news.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$news_id = $args['news_id'];
	$news = getNews($news_id,$loggedUser);
	return  $response->withJson($news);

})->add($jwtauth);

$app->put('/news', function ($request,$response) {
	require 'news.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$news =  updateNews($request,$loggedUser);
	return $response->withJson($news);
})->add($jwtauth);


$app->delete('/news/{news_id}', function ($request,$response,$args) {
	require 'news.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$news_id = $args['news_id'];
	$news = deleteNews($news_id,$loggedUser);
	return $response->withJson($news);

})->add($jwtauth);


$app->post('/project/{project_id}/news', function ($request,$response,$args) {
	require 'news.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$project_id = $args['project_id'];
	$request = $request->getParsedBody();
	$news = createNews($request,$loggedUser);
	return  $response->withJson($news);
})->add($jwtauth);



$app->get('/project', function ($request,$response) {
	require 'projects.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$projects = listProjects($loggedUser);
	return $response->withJson($projects);

})->add($jwtauth);


$app->get('/project/{project_id}', function ($request,$response,$args) {
	require 'projects.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$project_id = $args['project_id'];
	$project = getProject($project_id,$loggedUser);
	return $response->withJson($project);

})->add($jwtauth);



$app->get('/project/{project_id}/dataset', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$project_id = $args['project_id'];
	$datasets = listDatasets($project_id,$loggedUser);
	return $response->withJson($datasets);

})->add($jwtauth);

$app->post('/dataset', function ($request, $response) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$dataset = createDataset($request,$loggedUser);
	return $response->withJson($dataset);
})->add($jwtauth);

$app->put('/dataset', function ($request,$response) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$request = $request->getParsedBody();
	$dataset = updateDataset($request,$loggedUser);
	return $response->withJson($dataset);
})->add($jwtauth);


$app->get('/dataset/{dataset_id}', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$dataset_id = $args['dataset_id'];
	$dataset = getDataset($dataset_id,$loggedUser);
	return $response->withJson($dataset);;
})->add($jwtauth);



$app->delete('/dataset/{dataset_id}', function ($request, $response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$dataset_id = $args['dataset_id'];
	$dataset = deleteDataset($dataset_id,$loggedUser);
	return $response->withJson($dataset);

})->add($jwtauth);


$app->post('/dataset/{dataset_id}/upload', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$dataset_id = $args['dataset_id'];
	$dataset = $request->getParsedBody();
	$files = uploadInDataset($dataset_id,$request,$loggedUser);
	return $response->withJson($files);
})->add($jwtauth);

$app->get('/dataset/{dataset_id}/file/{file_id}', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$dataset_id = $args['dataset_id'];
	$file_id = $request->getAttribute('file_id');
	$file = downloadFile($dataset_id,$file_id,$loggedUser);
})->add($jwtauth);

$app->get('/dataset/{dataset_id}/file/{file_id}/view', function ($request, $response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$dataset_id = $args['dataset_id'];
	$file_id = $args['file_id'];
	$file = ViewFile($dataset_id,$file_id,$loggedUser);
	return $response->withJson($file);
})->add($jwtauth);

$app->get('/file/{filepath}', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$filepath = $args['filepath'];
	getFile($filepath,$loggedUser);
});


$app->delete('/file/{file_id}', function ($request,$response,$args) {
	require 'datasets.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$file_id = $args['file_id'];
	$files = deleteFile($file_id,$loggedUser);
	return $response->withJson($files);
})->add($jwtauth);

$app->get('/request/{action}/code/{code}', function ($request,$response,$args){
	require 'datasets.php';
	$action = $args['action'];
	$code = $args['code'];
	$message = handleRequest($action,$code);
	return withJson($message);
});

$app->get('/cv_permissions', function ($request,$response){
	require 'datasets.php';
	$permissions = listPermissions();
	return  $response->withJson($permissions);
})->add($jwtauth);

$app->get("/pubmed/{pmid}", function ($request,$response,$args){
	require 'publications.php';
	$pmid = $args['pmid'];
	$publi = getPubmed($pmid);
	return $response->withJson($publi);
})->add($jwtauth);

$app->get('/publication', function ($request,$response){
	require 'publications.php';
	$publications = listPublications();
	return $response->withJson($publications);
})->add($jwtauth);

$app->put('/publication', function ($request,$response){
	require 'publications.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$publication = $request->getParsedBody();
	$new_publication = updatePublication($publication,$loggedUser);
	return $response->withJson($new_publication);
})->add($jwtauth);

$app->post('/publication', function ($request,$response){
	require 'publications.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$publication = $request->getParsedBody();
	$new_publication = addPublication($publication,$loggedUser);
	return $response->withJson($new_publication);
})->add($jwtauth);


$app->delete('/publication/{id}', function ($request,$response,$args){
	require 'publications.php';
	$headers = $request->getHeaders();
	$loggedUser = $request->getAttribute('user');
	$publication_id = $args['id'];
	$publication_id = deletePublication($publication_id,$loggedUser);
	return $response->withJson($publication_id);
})->add($jwtauth);

$app->get('/download/{dataset_id}/{file_id}/{file_code}', function($request, $response, $args) {
    require 'datasets.php';
	$dataset_id = $args['dataset_id'];
	$file_id = $args['file_id'];
	$file_code = $args['file_code'];
	$file = geturl($dataset_id,$file_id,$file_code);
	$filename = basename("$file");
	$ctype = mime_content_type($file);
	header("Content-Length: " . filesize ( $file ) );
	header("Content-type: $ctype");
	header("Content-disposition: inline; filename=".basename($file));
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	ob_clean();
	flush();
	readfile($file);

});


$app->run();

?>