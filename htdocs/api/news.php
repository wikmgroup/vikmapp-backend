<?php

/**
 * check_news
 *
 * checks the validity of a news (contains title, content and author which is admin).
 *
 * @param string $news
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function check_news($news,$loggedUser){
	$is_admin = check_admin($loggedUser);
	if(!$is_admin) throw new Exception("Permission denied",501);
	$news['user_id'] = $GLOBALS['DB']->get("users","user_id",array("user_id" => $loggedUser->user_id));
	if(!$news['title'] || !$news['content'] || !$news['user_id']) throw new Exception("News invalid. Missing title or content", 501);
	return $news;
}

/**
 * listNews
 *
 * lists the news for a given user in a given project.
 *
 * @param int $project_id
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function listNews($project_id, $loggedUser){
	$is_admin = (strpos($loggedUser->permissions,'admin') !== FALSE);
	$project_id = check_project($project_id,$loggedUser->user_id);
	if(!$project_id) throw new Exception("Permission denied", 501);

	$fields = array(
		'users.user_id' => 'user_id',
		'users.firstname' => 'firstname',
		'users.lastname' => 'lastname',
		'news.title' => 'title',
		'news.content' => 'content',
		'news.project_id' => 'project_id',
		'date_format(news.timestamp,"%d.%m.%Y")' => 'timestamp',
		'news.expiration_date' => 'expiration_date',
		'news.is_active' => 'active'
	);
	$fields = array(
		'users.user_id (user_id)',
		'users.firstname (firstname)',
		'users.lastname (lastname)',
		'news.title (title)',
		'news.content (content)',
		'news.project_id (project_id)',
		'news.timestamp (timestamp)',
		'news.expiration_date (expiration_date)',
		'news.is_active (active)'
	);
	$where = ' and (news.expiration_date >= curdate() or news.expiration_date is null) ';
	$curdate = date('Y-m-d');
	$where = array(
		"AND" => array(
			"OR" => array(
				"project_id" => $project_id,
				"project_id" => null
			),
			"OR" => array(
				"#news.expiration_date[>=]" => $curdate,
				"news.expiration_date" => null
			)
		),
		"ORDER" => 'news.timestamp DESC'
	);

	if($is_admin){
		$fields[] = "news.news_id (news_id)";

	}
	else $where['AND']['news.is_active'] = 'Y';
	$news = $GLOBALS['DB']->select(
		"news",
		array(
			"[>]users" => "user_id"
		),
		$fields,
		$where
	);
	if(!$news) $news = array();
	foreach($news as $idx => $info){
		$news[$idx]['timestamp'] = date('d.m.Y',strtotime($info['timestamp']));
	}
	return $news;
}

/**
 * getNews
 *
 * restricted to *admin*. Gets all details of a news for edition.
 *
 * @param int $news_id
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function getNews($news_id,$loggedUser){
	$is_admin = (strpos($loggedUser->permissions,'admin') !== FALSE);
	if(!$is_admin) throw new Exception("Permission denied",501);
	$news = $GLOBALS['DB']->select(
	'news',
	array(
		"[>]users" => "user_id"
	),
	array(
		"news.news_id",
		"news.title",
		"news.content",
		"news.project_id",
		"news.timestamp",
		"news.user_id",
		"news.is_active",
		"users.firstname",
		"users.lastname",
		"news.expiration_date"
	),
	array(
		"news.news_id" => $news_id
	)
	);
	if(!count($news)) throw new Exception("No news can be found", 501);
	$theNews = $news[0];
	$theNews['timestamp'] = date('d.m.Y',strtotime($theNews['timestamp']));
	return $theNews;

}

/**
 * createNews
 *
 * Register a news. Restricted to admin via the check_news function.
 *
 * @param stdClass $news
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function createNews($news,$loggedUser){
	$news = check_news($news,$loggedUser);
	$newsletter = false;
	if($news->newsletter){ $newsletter = true; unset($news->newsletter);}

	$GLOBALS['DB']->insert('news',(array)$news);
	$news['news_id'] = $GLOBALS['DB']->id();
	$list = $GLOBALS['DB']->select(
		"news",
		array(
			"[><]users" => "user_id"
		),
		array(
			"firstname",
			"lastname",
			"news.news_id (news_id)",
			"news.user_id (user_id)",
			"news.title (title)",
			"news.content (content)",
			"news.timestamp (timestamp)",
			"news.is_active (active)",
			"news.project_id (project_id)",
			"news.expiration_date (expiration_date)"
		),
		array("news.news_id" => $news['news_id'])
	);

	if($newsletter){
		$user_newsletter = $GLOBALS['DB']->select(
			"users",
			array(
				"firstname",
				"lastname",
				"email"
			),
			array("users.newsletter" => 'Y')
		);

		$content = "Last news : ".$news['title']."\r\n\r\n";
		$content = $news['title'] . "\r\n\r\n";

		foreach($user_newsletter as $user){
			$headers = 'From: '. CONTACT_EMAIL . "\r\n" .
			    'Reply-To: '. CONTACT_EMAIL . "\r\n" .
			    'X-Mailer: PHP/' . phpversion();
			$out = mail($user['email'],'VIKM - newsletter ',$content,$headers);
		}
	}
	return (count($list)) ? $list[0] : array();
}

/**
 * updateNews
 *
 * Update a news. Restricted to admin via the check_news function.
 *
 * @param stdClass $news
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */


function updateNews($news,$loggedUser){
	$news = check_news($news,$loggedUser);
	$GLOBALS['DB']->update('news',array(
		'title' => $news['title'],
		'content' => $news['content'],
		'is_active' => $news['is_active'],
		'project_id' => $news['project_id'],
		'expiration_date' => $news['expiration_date']
	),array('news_id' => $news['news_id']));

	$list = $GLOBALS['DB']->select(
		"news",
		array(
			"[><]users" => "user_id"
		),
		array(
			"firstname",
			"lastname",
			"news.news_id",
			"news.user_id",
			"news.title",
			"news.content",
			"news.timestamp",
			"news.is_active",
			"news.project_id",
			"news.expiration_date"
		),
		array("news.news_id" => $news['news_id'])
	);

	return (count($list)) ? $list[0] : array();

}

/**
 * deleteNews
 *
 * deleted the news.
 *
 * @param id $new_id
 * @param stdClass $loggedUser * @return boolean
 * @author Robin
 */

function deleteNews($news_id,$loggedUser){
	$is_admin = check_admin($loggedUser);
	if(!$is_admin) throw new Exception("Permission denied",501);
	$GLOBALS['DB']->delete('news',array("news_id" => $news_id));
	return true;
}

?>