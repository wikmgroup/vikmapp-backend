<?php

/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/**
 * dl_file function
 *
 * Try to guest file type based on file extension. Set appropriate header and returns the file
 *
 * @param string $file
 * @param string $filename
 * @return data
 * @author Robin
 */

function dl_file($file,$filename=""){

    //First, see if the file exists
    if (!is_file($file)) { die("<b>404 File not found!</b>"); }

    //Gather relevent info about file
    $len = filesize($file);
    if(!$filename) $filename = basename($file);
    $file_extension = strtolower(substr(strrchr($filename,"."),1));

    //This will set the Content-Type to the appropriate setting for the file
    switch( $file_extension ) {
      case "pdf": $ctype="application/pdf"; break;
      case "exe": $ctype="application/octet-stream"; break;
      case "zip": $ctype="application/zip"; break;
      case "doc": $ctype="application/msword"; break;
      case "xls": $ctype="application/vnd.ms-excel"; break;
      case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
      case "gif": $ctype="image/gif"; break;
      case "png": $ctype="image/png"; break;
      case "jpeg":
      case "jpg": $ctype="image/jpg"; break;
      case "mp3": $ctype="audio/mpeg"; break;
      case "wav": $ctype="audio/x-wav"; break;
      case "mpeg":
      case "mpg":
      case "mpe": $ctype="video/mpeg"; break;
      case "mov": $ctype="video/quicktime"; break;
      case "avi": $ctype="video/x-msvideo"; break;

      //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
      case "php":
      case "htm":
      case "html": die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;

	  case "txt":
	  case "text":
	  case "fasta":
	  case "fas": $ctype="text/plain"; break;
      default: $ctype="application/force-download";
    }

    //Begin writing headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");

    //Use the switch-generated Content-Type
    header("Content-Type: $ctype");

    //Force the download
    $header='Content-Disposition: attachment; filename="'.$filename.'";';
    header($header );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$len);
    @readfile($file);
    exit;
}


/**
 * check_dataset_description
 *
 * Check if a user can read (accessLevel = 1) or edit (accessLevel = 2) and dataset.
 *
 * @param string|stdClass $dataset
 * @param string $loggedUserId
 * @param string $accessLevel
 * @return int
 * @author Robin
 */

function check_dataset_permissions($dataset,$loggedUserId,$accessLevel){
	$dataset_id = (is_numeric($dataset)) ? $dataset : $dataset['dataset_id'];
	if($dataset_id == -1) return true;
	// $userGroups = DB::queryFirstColumn("SELECT distinct group_id from user_groups inner join users on user_groups.user_id = users.user_id where users.user_id = %i",$loggedUserId);
	$userGroup = $GLOBALS['DB']->select("user_groups",array(
		"[><]users" => "user_id"
	),
	'group_id',
	array("users.user_id" => $loggedUserId)
	);
	$userGroups = array_unique($userGroup);
	$userProject = $GLOBALS['DB']->select("project_groups",array(
		"[><]user_groups" => "group_id"
		),
		"project_id",
		array("user_groups.user_id" => $loggedUserId)
	);
	$userProjects = array_unique($userProject);
	$dataset_id = $GLOBALS['DB']->get("datasets","dataset_id",array(
			"AND" => array(
				"OR" => array(
					"public_permissions[>=]" => $accessLevel,
					"AND #project permission" => array(
						"project_permissions[>=]" => $accessLevel,
						"project_id" => $userProjects
					),
					"AND #group permission" => array(
						"group_permissions[>=]" => $accessLevel,
						"group_id" => $userGroups
					),
					"user_id" => $loggedUserId
				),
				"dataset_id" => $dataset_id
			)
		)
	);
	return $dataset_id;
}

/**
 * check_download_permissions
 *
 * returns P if download request is pending.
 * returns N if download is forbidden.
 * returns R if download request has been rejected.
 * returns Y if download is allowed.
 *
 * @param int $loggedUserId
 * @param stdClass $dataset
 * @return char
 * @author Robin
 */

function check_download_permission($loggedUserId,$dataset){

	if($dataset['request_download'] == 'N') return 'Y';
	if($dataset['user_id'] == $loggedUserId) return 'Y';
	$is_admin = check_admin($loggedUser);
	if($is_admin) return 'Y';
	$user_groups = $GLOBALS['DB']->select("user_groups",'group_id',array("user_id" => $loggedUserId));
	if(in_array($dataset['group_id'],$user_groups)) return 'Y';

	$download_requests = $GLOBALS['DB']->select(
		"download_requests",
		array(
			"[><]datasets" => "dataset_id",
			"[>]users" => array("referer_id" => "user_id")
		),
		array(
			"download_request_id",
			"accept_date",
			"reject_date",
			"referer_id",
			"firstname",
			"lastname"
		),
		array(
			"AND" => array(
				"download_requests.dataset_id" => $dataset['dataset_id'],
				"download_requests.user_id" => $loggedUserId
			)
		)
	);
	if($download_requests && count($download_requests)){
		$download_request = $download_requests[0];
		if($download_request['accept_date']) return 'Y';
		elseif($download_request['reject_date']) return 'R';
		else return 'P';
	}
	return 'N';
}

/**
 * human_filesize
 *
 * Convert a filesize in bytes into a human readable filesize with n decimals.
 *
 * @param int $bytes
 * @param int $decimals
 * @return string
 * @author Robin
 */

function human_filesize($bytes, $decimals = 2) {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}



/**
 * human_mime_type
 *
 * Convert a mime type into a human readable file type
 *
 * @param string $mime_type
 * @return string
 * @author Robin
 */

function human_mime_type($mime_type) {
	$mime_types = array(
		"audio/aac" => "AAC audio",
		"application/x-abiword" => "AbiWord document",
		"application/octet-stream" => "Archive document (multiple files embedded)",
		"video/x-msvideo" => "AVI: Audio Video Interleave",
		"application/vnd.amazon.ebook" => "Amazon Kindle eBook format",
		"application/octet-stream" => "Any kind of binary data",
		"image/bmp" => "Windows OS/2 Bitmap Graphics",
		"application/x-bzip" => "BZip archive",
		"application/x-bzip2" => "BZip2 archive",
		"application/x-csh" => "C-Shell script",
		"text/css" => "Cascading Style Sheets (CSS)",
		"text/csv" => "Comma-separated values (CSV)",
		"application/msword" => "Microsoft Word",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document" => "Microsoft Word",
		"application/vnd.ms-fontobject" => "MS Embedded OpenType fonts",
		"application/epub+zip" => "Electronic publication (EPUB)",
		"application/ecmascript" => "ECMAScript (IANA Specification) (RFC 4329 Section 8.2)",
		"image/gif" => "Graphics Interchange Format (GIF)",
		"text/html" => "HyperText Markup Language (HTML)",
		"image/x-icon" => "Icon format",
		"text/calendar" => "iCalendar format",
		"application/java-archive" => "Java Archive (JAR)",
		"image/jpeg" => "JPEG image",
		"application/javascript" => "JavaScript (IANA Specification) (RFC 4329 Section 8.2)",
		"application/json" => "JSON format",
		"audio/midi audio/x-midi" => "Musical Instrument Digital Interface (MIDI)",
		"video/mpeg" => "MPEG Video",
		"application/vnd.apple.installer+xml" => "Apple Installer Package",
		"application/vnd.oasis.opendocument.presentation" => "OpenDocument presentation document",
		"application/vnd.oasis.opendocument.spreadsheet" => "OpenDocument spreadsheet document",
		"application/vnd.oasis.opendocument.text" => "OpenDocument text document",
		"audio/ogg" => "OGG audio",
		"video/ogg" => "OGG video",
		"application/ogg" => "OGG",
		"font/otf" => "OpenType font",
		"image/png" => "PNG image",
		"application/pdf" => "PDF",
		"application/vnd.ms-powerpoint" => "Microsoft PowerPoint",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation" => "Microsoft PowerPoint",
		"application/x-rar-compressed" => "RAR archive",
		"application/rtf" => "Rich Text Format (RTF)",
		"application/x-sh" => "Bourne shell script",
		"image/svg+xml" => "Scalable Vector Graphics (SVG)",
		"application/x-shockwave-flash" => "Small web format (SWF) or Adobe Flash document",
		"application/x-tar" => "Tape Archive (TAR)",
		"image/tiff" => "Tagged Image File Format (TIFF)",
		"application/typescript" => "Typescript file",
		"font/ttf" => "TrueType Font",
		"text/plain" => "Text",
		"application/vnd.visio" => "Visio",
		"audio/wav" => "Waveform Audio Format",
		"audio/webm" => "WEBM audio",
		"video/webm" => "WEBM video",
		"image/webp" => "WEBP image",
		"font/woff" => "Web Open Font Format (WOFF)",
		"font/woff2" => "Web Open Font Format (WOFF)",
		"application/xhtml+xml" => "XHTML",
		"application/vnd.ms-excel" => "Microsoft Excel",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "Microsoft Excel",
		"application/xml" => "XML",
		"application/vnd.mozilla.xul+xml" => "XUL",
		"application/zip" => "ZIP archive",
		"video/3gpp" => "3GPP audio/video container",
		"video/3gpp2" => "3GPP2 audio/video container",
		"7-zip archive" => "application/x-7z-compressed"
	);
	return (isset($mime_types[$mime_type])) ? $mime_types[$mime_type] : $mime_type;
}


/**
 * get_files
 *
 * lists files of an dataset. Check if file exist on the filesystem. Reads the content of a readme file.
 *
 * @param int $dataset_id
 * @return array(files array, readme string)
 * @author Robin
 */

function get_files($dataset_id){
	$return = array('files' => array(),'readme' => '');
	$project_id = $GLOBALS['DB']->get("datasets",'project_id',array("dataset_id" => $dataset_id));
	$return['files'] = $GLOBALS['DB']->select(
		"files",
		array(
			"[>]users" => "user_id"
		),
		array(
			"name",
			'firstname',
			'lastname',
			"size",
			"mime_type",
			"timestamp",
			"file_id",
			"access_code"
		),
		array(
			'AND' => array(
				"files.is_deleted" => "N",
				"dataset_id" => $dataset_id
			)
		)
	);
	if(!$return['files']) $return['files'] = array();
	foreach($return['files'] as $idx => $file){
		$path = DATA_PATH."/project_".$project_id."/dataset_".$dataset_id."/".$file['name'];
		if(!file_exists($path)){
			unset($return['files'][$idx]);
			continue;
		}
		$return['files'][$idx]['size'] = human_filesize($file['size']);
		$return['files'][$idx]['mime_type'] = human_mime_type($file['mime_type']);
		$return['files'][$idx]['user_name'] = $file['firstname']." ".$file['lastname'];
		unset($return['files'][$idx]['firstname']);
		unset($return['files'][$idx]['lastname']);
		$return['files'][$idx]['timestamp'] = date('d.m.Y',strtotime($file['timestamp']));
		$basename = pathinfo($file['name'], PATHINFO_FILENAME);
		if(strtolower($basename) === 'readme'){
			$return['readme'] = (file_get_contents($path));
		}
	}
	$return['files'] = array_values($return['files']);
	return $return;
}

/**
 * listDatasets
 *
 * Lists the datasets from a given project accessible to a given user.
 *
 * @param int $project_id
 * @param stdClass $loggedUser
 * @param string $loggedCode
 * @return array
 * @author Robin
 */

function listDatasets($project_id,$loggedUser){
	$is_admin = check_admin($loggedUser);
	$project_id = check_project($project_id,$loggedUser->user_id);
	if(!$project_id && !$is_admin) throw new Exception("Permission denied", 501);

	$fields = array(
		'distinct datasets.dataset_id' => 'dataset_id',
		'users.user_id' => 'user_id',
		'users.firstname' => 'firstname',
		'users.lastname' => 'lastname',
		'groups.name' => 'group_name',
		'projects.name' => 'project_name',
		'datasets.name' => 'name',
		'datasets.description' => 'description',
		'datasets.request_download' => 'request_download',
		'datasets.group_permissions' => 'group_permissions',
		'datasets.project_permissions' => 'project_permissions',
		'datasets.public_permissions' => 'public_permissions',
		"datasets.timestamp" => 'timestamp'
	);
	$where = " where datasets.project_id = ".(intval($project_id))." and  datasets.status = 'valid' and (datasets.public_permissions > 0 or (datasets.project_id = logged_projects.project_id and datasets.project_permissions > 0) or (datasets.group_id = logged_groups.group_id and datasets.group_permissions > 0) or logged_user.user_id = datasets.user_id)";
	if($is_admin){
		$fields['datasets.status'] = 'active';
		$where = " where  datasets.status = 'valid' and datasets.project_id =  ".(intval($project_id));
	}
	$query = "SELECT ";
	foreach($fields as $sqlfield => $field){
		$query .= $sqlfield." as ".$field.", ";
	}
	$query = rtrim($query,', ');
	$query .= " from datasets
		left join users on datasets.user_id = users.user_id
		left join groups on datasets.group_id = groups.group_id
		left join projects on datasets.project_id = projects.project_id
		left join users as logged_user on logged_user.user_id = ".$GLOBALS['DB']->quote($loggedUser->user_id)."
		left join user_groups as logged_groups on logged_user.user_id = logged_groups.user_id
		left join project_groups as logged_projects on logged_projects.group_id = datasets.group_id";
	$query .= $where;
	$datasets = $GLOBALS['DB']->query($query)->fetchAll();
	foreach($datasets as $idx => $dataset){
		$datasets[$idx]['user_name'] = $dataset['firstname']." ".$dataset['lastname'];
		unset($dataset['firstname']);
		unset($dataset['lastname']);
		$datasets[$idx]['timestamp'] = date('d.m.Y',strtotime($dataset['timestamp']));
	}
	return $datasets;
}


/**
 * createDataset
 *
 * register a new dataset
 *
 * @param stdClass $dataset
 * @param stdClass $loggedUser * @return stdClass
 * @author Robin
 */

function createDataset($dataset,$loggedUser){
	if(!$dataset['name']) throw new Exception("Dataset name is missing", 501);
	$loggedUserId = $loggedUser->user_id;
	if(!check_dataset_permissions($dataset,$loggedUserId,2)) throw new Exception("Permission denied", 501);
	$GLOBALS['DB']->insert('datasets',array(
		'name' => $dataset['name'],
		'description' => $dataset['description'],
		'project_id' => $dataset['project_id'],
		'user_id' => $loggedUserId,
		'group_id' => $dataset['group_id'],
		'project_permissions' => $dataset['project_permissions'],
		'group_permissions' => $dataset['group_permissions'],
		'public_permissions' => $dataset['public_permissions'],
		'request_download' => $dataset['request_download'],
		'status' => 'valid'
	));
	$dataset['dataset_id'] = $GLOBALS['DB']->id();
	return $dataset;
}


/**
 * getDataset
 *
 * gets details of an dataset. Returns edit and download permissions for the current user.
 *
 * @param int $dataset_id
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function getDataset($dataset_id,$loggedUser){
	$is_admin = check_admin($loggedUser);
	$loggedUserId = $loggedUser->user_id;
	if(!check_dataset_permissions($dataset_id,$loggedUserId,1)) throw new Exception("Permission denied", 501);
	$fields = array(
		'datasets.dataset_id (dataset_id)',
		'users.user_id (user_id)',
		'users.firstname (firstname)',
		'users.lastname (lastname)',
		'groups.name (group_name)',
		'datasets.project_id (project_id)',
		'projects.name (project_name)',
		'datasets.name (name)',
		'datasets.description (description)',
		'datasets.request_download (request_download)',
		'datasets.link_download (link_download)',
		'datasets.group_id (group_id)',
		'datasets.group_permissions (group_permissions)',
		'datasets.project_permissions (project_permissions)',
		'datasets.public_permissions (public_permissions)',
		'timestamp (timestamp)'
	);

	if($is_admin){
		$fields[] = 'datasets.status (status)';
	}


	$datasets = $GLOBALS['DB']->select(
		"datasets",
		array(
			"[><]users" => array("datasets.user_id" => "user_id"),
			"[><]groups" => array("datasets.group_id" => "group_id"),
			"[><]projects" => array("datasets.project_id" => "project_id")
		),
		$fields,
		array("AND" => array("datasets.dataset_id" => $dataset_id,"datasets.status" => "valid"))
	);



	if($datasets && count($datasets)){
		$dataset = $datasets[0];
		$dataset['logged_permission'] = 1;
		$dataset['user_name'] = $dataset['firstname']." ".$dataset['lastname'];
		unset($dataset['firstname']);
		unset($dataset['lastname']);
		$dataset['timestamp'] = date('d.m.Y',strtotime($dataset['timestamp']));
		$dataset['logged_request_download'] = check_download_permission($loggedUserId,$dataset);
		if(check_dataset_permissions($dataset_id,$loggedUserId,2) || $is_admin) 	$dataset['logged_permission'] = 2;


		$files = get_files($dataset_id);
		$dataset['files']  = $files['files'];
		$dataset['readme'] = $files['readme'];
		return $dataset;
	}
	else throw new Exception("Dataset is undefined", 501);

}


 /**
 * updateDataset
 *
 * Function to update an dataset. Check permission first !!
 *
 * @param stdClass $dataset
 * @param stdClass $loggedUser * @return void
 * @author Robin
 */

 function updateDataset($dataset,$loggedUser){
 	$loggedUserId = $GLOBALS['DB']->get("users","user_id",array("user_id" => $loggedUser->user_id));
 	if(!check_dataset_permissions($dataset['dataset_id'],$loggedUserId,2)) throw new Exception("Permission denied", 501);
 	if(!$dataset['name']) throw new Exception("Dataset Name is required !!", 501);
 	$GLOBALS['DB']->update('datasets',array(
 	        'name' => $dataset['name'],
 	        'description' => $dataset['description'],
 	        'group_permissions' => $dataset['group_permissions'],
 	        'link_download' => $dataset['link_download'],
 	        'project_permissions' => $dataset['project_permissions'],
 	        'public_permissions' => $dataset['public_permissions'],
 	        'request_download' => $dataset['request_download']
 	        ),
 	        array(
 	                'dataset_id' => $dataset['dataset_id']
 	        )
 	);

	$files = get_files($dataset['dataset_id']);
	if($dataset['readme'] && (!isset($files['readme']) || $dataset['readme'] != $files['readme'])){
		$project_id = $GLOBALS['DB']->get("datasets",'project_id',array("dataset_id" => $dataset['dataset_id']));
		foreach($files['files'] as $file){
			$basename = pathinfo($file['name'], PATHINFO_FILENAME);
			if(strtolower($basename) === 'readme'){
				$path = DATA_PATH."/project_".$project_id."/dataset_".$dataset['dataset_id']."/".$file['name'];
				file_put_contents($path,$dataset['readme']);
			}
		}
	}


 	return $dataset;
  }


/**
 * deleteDataset
 *
 * Function to delete an dataset. Doesn't delete it, but simply change its status to _deleted_
 *
 * @param int $dataset_id
 * @param stdClass $loggedUser * @return boolean
 * @author Robin
 */

function deleteDataset($dataset_id,$loggedUser){
	$is_admin = check_admin($loggedUser);
	$loggedUserId = $loggedUser->user_id;
	if(!check_dataset_permissions($dataset_id,$loggedUserId,2)) throw new Exception("Permission denied", 501);
	$GLOBALS['DB']->update("files",array('is_deleted' => 'Y'),array("dataset_id" => $dataset_id));
	return $GLOBALS['DB']->update("datasets",array('status' => 'deleted'),array("dataset_id" => $dataset_id));
}

/**
 * uploadInDataset
 *
 * upload a new file in an dataset. Check if the logged user is allowed to edit the dataset.
 *
 * @param int $dataset_id
 * @param stdClass $request. Probably not used
 * @param stdClass $loggedUser * @return void
 * @author Robin
 */

function uploadInDataset($dataset_id,$request,$loggedUser){
	if(!isset($_FILES['file'])) throw new Exception("No file received", 501);
	$loggedUserId = $GLOBALS['DB']->get("users","user_id",array("user_id" => $loggedUser->user_id));
	if(!check_dataset_permissions($dataset_id,$loggedUserId,2)) throw new Exception("Permission denied", 501);
	$dataset = $GLOBALS['DB']->get("datasets",array("dataset_id","project_id"),array("dataset_id" => $dataset_id));
	$dir = DATA_PATH."/project_".$dataset['project_id']."/dataset_".$dataset_id;
	if(!file_exists($dir)) mkdir($dir,0755,true);
	$filename = $_FILES['file']['name'];
	$testidx = 0;
	while(file_exists($dir."/".$filename) && $testidx < 100){
		$testidx++;
		$basename = pathinfo($filename, PATHINFO_FILENAME);
		if(preg_match("/(.*)\.(\d+)$/",$basename,$regs)){
			$filename = $regs[1].".".(intval($regs[2])+1).".".pathinfo($filename, PATHINFO_EXTENSION);
		}
		else $filename = $basename.".1.".pathinfo($filename, PATHINFO_EXTENSION);
	}
	if(!is_writeable($dir)) throw new Exception("ERROR: $dir is not writeable", 501);

	if(!move_uploaded_file($_FILES['file']['tmp_name'],$dir."/".$filename)){
		error_log($_FILES["file"]["error"]);
		throw new Exception("Sorry, upload error2 to $dir/$filename !", 501);
	}
	$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
	$type = finfo_file($finfo, $dir."/".$filename);
	$size = filesize($dir."/".$filename);
	finfo_close($finfo);
	$GLOBALS['DB']->insert('files',array(
		'name' => $filename,
		'user_id' => $loggedUserId,
		'dataset_id' => $dataset_id,
		'size' => $size,
		'mime_type' => $type,
		'access_code' => rand_str()
	));
	$file_id = $GLOBALS['DB']->id();
	$files = get_files($dataset_id);
	return $files;

}

/**
 * deleteFile
 *
 * To delete a file in an dataset. Does NOT delete the file. Move it to a _deleted_files_ directory and set _is_deleted_ to _Y_
 *
 * @param int $file_id
 * @param stdClass $loggedUser * @return void
 * @author Robin
 */

function deleteFile($file_id,$loggedUser){
	$loggedUserId = $GLOBALS['DB']->get("users","user_id",array("user_id" => $loggedUser->user_id));
	$files = $GLOBALS['DB']->select(
	"files",
	array(
		"[><]datasets" => "dataset_id"
	),
	array(
		"file_id",
		"files.name",
		"files.user_id",
		"files.dataset_id",
		"datasets.project_id",
		"files.is_deleted"
	),
	array("files.file_id" => $file_id)
	);

	if(!count($files)) throw new Exception("File unknown", 501);
	$file = $files[0];
	if($file['is_deleted'] == 'Y') throw new Exception("File ".$file['name']." is already deleted", 501);

	if(!check_dataset_permissions($file['dataset_id'],$loggedUserId,2)) throw new Exception("Permission denied", 501);
	$dir = DATA_PATH."/project_".$file['project_id']."/dataset_".$file['dataset_id'];
	if(!file_exists($dir."/".$file['name'])) throw new Exception("File could not be found", 501);
	if(!file_exists($dir."/deleted_files")) mkdir($dir."/deleted_files",0755,true);
	if(!rename($dir."/".$file['name'],$dir."/deleted_files/".$file['name'])) throw new Exception("Could not move file to deleted_files directory", 501);
	$GLOBALS['DB']->update(
		"files",
		array("is_deleted" => "Y"),
		array("file_id" => $file_id)
	);
	return get_files($file['dataset_id']);
}

/**
 * downloadFile
 *
 * check if a file can be downloaded. Sends an email if a request is needed. Return the path to the file, the status of the request.
 *
 * @param int $dataset_id
 * @param int $file_id
 * @param stdClass $loggedUser * @return array
 * @author Robin (and Fabio)
 */

function downloadFile($dataset_id,$file_id,$loggedUser){

	$return = array(
		'path' => '',
		'message' => '',
		'requested' => false
	);
	$loggedUser = $GLOBALS['DB']->get("users",array("user_id",'firstname','lastname','email'),array("user_id" => $loggedUser->user_id));
	$loggedUserId = $loggedUser['user_id'];
	if(!check_dataset_permissions($dataset_id,$loggedUserId,1)) throw new Exception("Permission denied", 501);
	$datasets = $GLOBALS['DB']->select(
		"datasets",
		array(
			"[><]projects" => "project_id"
		),
		array(
			"datasets.dataset_id (dataset_id)",
			"datasets.project_id (project_id)",
			"datasets.group_id (group_id)",
			"datasets.user_id (user_id)",
			"datasets.name (name)",
			"datasets.request_download (request_download)",
			"projects.name (project_name)"
		),
		array(
			"datasets.dataset_id" => $dataset_id
		)
	);
	if(!count($datasets)) throw new Exception("Dataset unknown", 501);
	$dataset = $datasets[0];
	$permission = check_download_permission($loggedUserId,$dataset);
	if($permission === 'N'){
		$datasetUsers = $GLOBALS['DB']->select(
			"users",
			array(
				"[><]datasets" => "user_id"
			),
			array(
				"users.user_id",
				"firstname",
				"lastname",
				"email",
				"code"
			),
			array(
				"datasets.dataset_id" => $dataset_id
			)
		);
		if(!count($datasetUsers)) throw new Exception("ERROR, no user for this dataset", 501);
		$datasetUser = $datasetUsers[0];
		$request = $GLOBALS['DB']->get('download_requests',array('code','request_date'),array("AND" => array("dataset_id" => $dataset_id,"user_id" => $loggedUserId)));
		$code = ($request) ? $request['code'] : null;
		if(!$code){
			$code = rand_str();
			$code64 = base64_encode($datasetUser['code'].":".$code);
			$request_date = date('Y-m-d h:i:s');
			$GLOBALS['DB']->insert('download_requests',array('user_id' => $loggedUserId, 'dataset_id' => $dataset_id, 'code' => $code,'request_date' => $request_date));
			$diff = 1441;
		}
		else{
			$now = time();
			$request_date = strtotime($request['request_date']);
			$diff = round(($now-$request_date)/60);
		}
		if($diff > 1440){ // sends an email if the request is new or the request has been done more than 24 hours ago (24*60)
			$title = "[".SITE_TITLE."] File download request";
			$body = "Dear ".$datasetUser['firstname'].", \r\n\r\n";
			$body .= $loggedUser['firstname']." ".$loggedUser['lastname']." (".$loggedUser['email'].") has requested a download access to the dataset ".$dataset['name']." of the project ".$dataset['project_name'].".\r\n\r\n";
			$body .= "To validate the access, please click on the following link: http://".$_SERVER['SERVER_NAME'].'/#/request/accept/'.$code64."\r\n\r\n";
			$body .= "To reject the access, please click on the following link: http://".$_SERVER['SERVER_NAME'].'/#/request/reject/'.$code64."\r\n\r\n";
			$body .= "Thank you for your cooperation.\r\n\r\n";
			$headers =    "MIME-Version: 1.0\r\n" .
			               "Content-type: text/plain; charset=utf-8; format=flowed\r\n" .
			               "Content-Transfer-Encoding: 8bit\r\n" .
						   "From: ".CONTACT_EMAIL ."\r\n" .
			               "X-Mailer: PHP" . phpversion();
			mail($datasetUser['email'],$title,$body,$headers);
		}
		$return['message'] = 'The access has been requested to '.$datasetUser['firstname']." ".$datasetUser['lastname'].". You will receive an email when it is reviewed.";
		$return['requested'] = true;
	}
	elseif($permission === 'R'){
		$return['message'] = "Your download request has been rejected";
	}
	elseif($permission === 'P'){
		$return['message'] = "Your download request is pending";
		$return['requested'] = true;
	}
	elseif($permission === 'Y'){
		$dir = DATA_PATH."/project_".$dataset['project_id']."/dataset_".$dataset_id;
		$prefix = preg_replace("/[^a-zA-Z0-9]/","",SITE_TITLE);
		if($file_id == 'all'){
			$filepath = tempnam(TMP_PATH,$prefix);
			$filename = $prefix."_archive_exp".$dataset_id.".zip";
			$files = $GLOBALS['DB']->select("files","name",array("AND" => array("is_deleted" => "N","dataset_id" => $dataset_id)));
			$filepath .= ".zip";
			foreach($files as $file){

				if(file_exists($dir."/".$file)){
					exec("zip -j -g ".$filepath." '".$dir."/".$file."'");
				}
				else error_log($dir."/".$file." does not exist");
			}
		}
		else if (preg_match(('/^\d+(?:,\d+)/'),$file_id)) {

			$filepath = tempnam(TMP_PATH,$prefix);
			$filename = $prefix."_archive_exp".$dataset_id.".zip";
			$file_ids=preg_split("/[\s,]+/",$file_id);
			$filepath .= ".zip";
			foreach($file_ids as $fid){

				$file_name = $GLOBALS['DB']->select('files',"name",array("AND" => array("is_deleted" => "N","file_id" =>$fid)));

				if(file_exists($dir."/".$file_name[0])){
					exec("zip -j -g ".$filepath." '".$dir."/".$file_name[0]."'");
				}
				else error_log($dir."/".$file_name[0]." does not exist");
			}
		}
		else{
			$filepath = TMP_PATH."/".$prefix.(rand_str(12));
			$files = $GLOBALS['DB']->select(
				"files",
				array(
					"[><]datasets" => "dataset_id"
				),
				array(
					"file_id (file_id)",
					"files.name (name)",
					"files.user_id (user_id)",
					"files.dataset_id (dataset_id)",
					"datasets.project_id (project_id)",
					"files.is_deleted (is_deleted)"
				),
				array(
					"files.file_id" => $file_id
				)
			);
			if(!count($files)) throw new Exception("File unknown", 501);
			$file = $files[0];
			if($file['is_deleted'] == 'Y') throw new Exception("File ".$file['name']." is already deleted", 501);
			if($dataset_id != $file['dataset_id']) throw new Exception("Oups, this should not occur. File dataset_id is not correct", 1);
			symlink($dir."/".$file['name'],$filepath);
			$filename = $file['name'];
		}
		if(!file_exists($filepath)) throw new Exception("File $filepath could not be found", 501);
		$comment = 'File';
		$comment .= ($file_id == 'all')?"s".(implode(", ",$files)):" ".$filename." of dataset_id = $dataset_id";
		$GLOBALS['DB']->insert('logs',array(
			'user_id' => $loggedUserId,
			'action' => 'download',
			'ip_address' => $_SERVER['REMOTE_ADDR'],
			'comment' => $comment
		));
		$log_id = $GLOBALS['DB']->id();
		$return['path']	 = base64_encode(basename($filepath).":".$filename);
	}
	else throw new Exception("Sorry, undefined dataset permission...", 501);

	echo json_encode($return);
}

/**
 * getFile
 *
 * checks if a file exists and download it if yes.
 *
 * @param string $path
 * @return void
 * @author Robin
 */

function getFile($path){
	$parts = explode(":",base64_decode($path),2);
	$filename = $parts[1];
	$filepath = TMP_PATH."/".$parts[0];
	if(file_exists($filepath)){
		dl_file($filepath,$filename);
		if(substr($filepath,0,strlen(TMP_PATH)) == TMP_PATH) unlink($filepath);
	}
}


/**
 * handleRequest
 *
 * handle a download request. From the links sent to the file owner by email.
 *
 * @param string $action
 * @param string $code
 * @return array
 * @author Robin
 */

function handleRequest($action,$code){
	$message = array('type' => 'success','message' => '');
	$codes = explode(":",base64_decode($code));
	$validator_code = $codes[0];
	$request_code = $codes[1];
	$validator = $GLOBALS['DB']->get(
		"users",
		array(
			"user_id",
			"firstname",
			"lastname",
			"email"
		),
		array("code" => $validator_code)
	);
	if(!$validator) throw new Exception("ERROR: validator code is not valid", 501);

	$requests = $GLOBALS['DB']->select(
		"download_requests",
		array(
			"[><]users" => "user_id",
			"[><]datasets" => "dataset_id",
			"[><]projects" => "project_id"
		),
		array(
			"download_request_id",
			"firstname",
			"lastname",
			"email",
			"datasets.name (dataset_name)",
			"projects.name (project_name)"
		),
		array("download_requests.code" => $request_code)
	);
	if(!count($requests)) throw new Exception("ERROR: request code is not valid", 501);
	$request = $requests[0];

	$key = $action."_date";
	$now = date('Y-m-d H:i:s');
	$GLOBALS['DB']->update("download_requests",array($key => $now,'referer_id' => $validator['user_id']),array('download_request_id' => $request['download_request_id']));
	$title = "[".SITE_TITLE."] File download request ".$action."ed";
	$body = "Dear ".$request['firstname'].", \r\n\r\n";
	$body .= $validator['firstname']." ".$validator['lastname']." (".$validator['email'].") has ".$action."ed your download access to the dataset ".$request['dataset_name']." of the project ".$request['project_name'].".\r\n\r\n";
	$body .= "Thank you for your patience.\r\n\r\n";
	$headers =    "MIME-Version: 1.0\r\n" .
	               "Content-type: text/plain; charset=utf-8; format=flowed\r\n" .
	               "Content-Transfer-Encoding: 8bit\r\n" .
				   "From: ".CONTACT_EMAIL ."\r\n" .
	               "X-Mailer: PHP" . phpversion();
	mail($request['email'],$title,$body,$headers);

	$message['type'] = ($action == 'accept') ? 'success' : 'danger';
	$message['message'] = "Thank you. The download access for ".$request['firstname']." ".$request['lastname']." to the dataset ".$request['dataset_name']." of the project ".$request['project_name']." has been successfuly ".$action."ed. An email has been sent to ".$request['email'];

	return $message;

}

function listPermissions(){
	$permissions = $GLOBALS['DB']->select(
		"cv_permissions",
		array(
			'permission_id (id)',
			'description (name)'
		)
	);
	return array_map(function($perm){
		$perm['id'] = +$perm['id'];
		return $perm;
	},$permissions);
}

/**
 * ViewFile
 *
 * allow to view excel like file.
 *if image (png or jpg) send an array with "img" as type
 *
 * @param int $dataset_id
 * @param int $file_id
 * @param stdClass $loggedUser * @return array
 * @author Fabio
 */
function ViewFile($dataset_id,$file_id,$loggedUser){
	$loggedUser = $GLOBALS['DB']->get("users",array("user_id",'firstname','lastname','email'),array("user_id" => $loggedUser->user_id));
	$loggedUserId = $loggedUser['user_id'];
	if(!check_dataset_permissions($dataset_id,$loggedUserId,1)) throw new Exception("Permission denied", 501);

	$file_info = $GLOBALS['DB']->select('files','*',["file_id" => $file_id]);
	$project_id = $GLOBALS['DB']->get("datasets",'project_id',array("dataset_id" => $dataset_id));

	$path = DATA_PATH."/project_".$project_id."/dataset_".$dataset_id."/".$file_info[0]['name'];


	$path_parts = pathinfo($path);
	$extension= $path_parts['extension'];
	if(getimagesize($path)){
		$type= "img";
		$sheet = array('type'=>$type,'name'=>$file_info[0]['name']);
	}

	// if($extension =='jpg' || $extension == 'png' || $extension == 'jpeg'){
	// }

	else if($extension =='txt' || $extension == 'md'){
		$type= $extension;
		$content = file_get_contents($path);
		$sheet = array('type'=>$type,'content'=>$content,'row'=>array(), 'name'=>$file_info[0]['name']);
	}

	else if($extension =='pdf'|| $extension == 'ods' || $extension == 'odp'){
		$type= "viewer";
		$sheet = array('type'=>$type, 'name'=>$file_info[0]['name']);
	}

	else if($extension =='md'){
		if (file_exists(GRIP_PATH)){

			$type= "md";
			$TmpFileName=rand_str();
			$link = DATA_PATH.'/'.$TmpFileName.'.md';
			symlink($path,$link);
			exec(GRIP_PATH. ' --export --title "'.$file_info[0]['name'].'" '.$link);
			$html = str_replace('md','html',$link);
			$content = file_get_contents($html);
			$start = strpos($content,'<body>');
			$end = strpos($content,"</body>");
			if($start && $end) $content = substr($content,$start+6,$end-$start+6);
			unlink($html);
			unlink($link);

		}
		else{
			$type= "txt";
			$content = file_get_contents($path);

		}

		$sheet= array('type'=>$type,'content'=>$content,'row'=>array(), 'name'=>$file_info[0]['name']);
	}

	elseif($extension == 'docx'){
		if(defined("PANDOC_PATH")){
			$pandoc_path = PANDOC_PATH;
		}
		else{
			$pandoc_path = exec("which pandoc");
		}
		if($pandoc_path){
			$cmd = $pandoc_path." -f docx -t markdown ".$path;
			$output = exec($cmd,$outs);
			$content = implode("\n",$outs);
		}
		else $content = '';
		$sheet= array('type'=>'docx','content'=>$content,'row'=>array(), 'name'=>$file_info[0]['name']);
	}

	else if($extension =='csv'){

		function getFileDelimiter($file, $checkLines = 2){
		        $file = new SplFileObject($file);
		        $delimiters = array(
		          ',',
		          '\t',
		          ';',
		          '|',
		          ':'
		        );
		        $results = array();
		        $i = 0;
		         while($file->valid() && $i <= $checkLines){
		            $line = $file->fgets();

					if ($line[0] !== '#'){

		            foreach ($delimiters as $delimiter){
		                $regExp = '/['.$delimiter.']/';
		                $fields = preg_split($regExp, $line);

		                if(count($fields) > 1){
		                    if(!empty($results[$delimiter])){
		                        $results[$delimiter]++;
		                    } else {
		                        $results[$delimiter] = 1;
		                    }
		                }
		            }
		           $i++;
			   }
		        }
		        $results = array_keys($results, max($results));

		        return $results[0];
		    }

	$delimiters = getFileDelimiter($path, 1);
	$file_csv = fopen($path, 'r');
	$type= $extension;
	$comment= [];
	$header=[];
	$row=[];
	if($delimiters=='\\t'){
	$delimiters= "\t";}
		;
		while (($line = fgetcsv($file_csv, 0, $delimiters)) !== FALSE) {
			if($line[0][0] == '#'){

				$comment[]=implode(",", $line);
			}

			else{
				if(empty($header)){
				$header[]=$line;}
				else{
					$row[]=$line;
				}
			}
		}
	fclose($file_csv);
	$sheet = array('type'=>$type,'header'=>$header,'row'=>$row, 'name'=>$file_info[0]['name'],'comment'=>$comment);

	}

	else{
		error_reporting(E_ALL);
		set_time_limit(0);
		date_default_timezone_set('Europe/London');
	       $inputFileName = $path;
 		   $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
 		   $objWriter = new PHPExcel_Writer_HTML($objPHPExcel);
 		   $objWriter->writeAllSheets();
		   $TmpFileName=rand_str(30);
		   $link = $TmpFileName.'.html';
 		   $objWriter->save($link);
 		   $content = file_get_contents($link);
		   unlink($link);

 		   $dom = new DOMDocument;
 		   $dom->loadHTML($content);
 		   $tables = array();
 		   $domtables = $dom->getElementsByTagName('table');
 		   foreach($domtables as $domtable)
 		   {
 			   $domtable->setAttribute('class', 'table table-bordered' . $domtable->getAttribute(''));
 		       $tables[] = $dom->saveHTML($domtable);
 		   }

 		   $headNode = $dom->getElementsByTagName('head');
 		   foreach ($headNode as $node) {
 			   $test= $dom->saveHTML($node);
 			   $dom1 = new DOMDocument;
 			   $dom1->loadHTML($test);
 			   $test2 = $dom1->getElementsByTagName('style');
 			   foreach ($test2 as $test3) {
 				   $style= preg_replace("/\s/","",$test3->nodeValue);
 			   }
 		   }

 		   $title=array();

 		   $domsheets = $dom->getElementsByTagName('ul');
 		   foreach($domsheets as $domsheet){
 		       $titles= $dom->saveHTML($domsheet);
 			   $dom2= new DOMDocument;
 			   $dom2->loadHTML($titles);
 			   $aNodes = $dom2->getElementsByTagName('a');
 			   foreach($aNodes as $aNode){

 			   	   $title[]= $aNode->nodeValue;
 			   }
 		   }

   		   $sheet = array('type'=>'xls','tables'=>$tables, 'name'=>$file_info[0]['name'],'style'=>$style,'titles'=>$title);

		}


	return  $sheet;

};
function geturl($dataset_id,$file_id,$file_code){
	$access_code = $GLOBALS['DB']->get("files","access_code",array("file_id" => $file_id));
	if($access_code == $file_code){
		$file_info = $GLOBALS['DB']->select('files','*',["file_id" => $file_id]);
		$project_id = $GLOBALS['DB']->get("datasets",'project_id',array("dataset_id" => $dataset_id));
		$path = DATA_PATH."/project_".$project_id."/dataset_".$dataset_id."/".$file_info[0]['name'];
		return $path;
	}
	else{ throw new Exception("Permission denied", 501);

	}

}
?>
