<?php

function getPublication($publication_id){
	$publication = $GLOBALS['DB']->get("publications",array("[><]users" => array("user_id" => "user_id")),array("users.firstname", "users.lastname","users.user_id", "publications.publication_id","publications.title","publications.authors","publications.journal","publications.volume","publications.page","publications.year","publications.doi","publications.pmid","publications.url","publications.abstract","publications.date"),array('publication_id'=>$publication_id));
	$publication['year'] = +$publication['year'];
	$publication['username'] = substr($publication['firstname'],0,1) . ". " . $publication['lastname'];
	return $publication;
}

function listPublications(){
	$publications = array();
	$tmp_publications = $GLOBALS['DB']->select("publications",array("[><]users" => array("user_id" => "user_id")),array("users.firstname", "users.lastname","users.user_id", "publications.publication_id","publications.title","publications.authors","publications.journal","publications.volume","publications.page","publications.year","publications.doi","publications.pmid","publications.url","publications.abstract","publications.date"));
	foreach($tmp_publications as $p){
		$p['year'] = +$p['year'];
		$p['username'] = substr($p['firstname'],0,1) . ". " . $p['lastname'];
		unset($p['firstname']);
		unset($p['lastname']);
		$publications[] = $p;
	}
	return $publications;
}

function addPublication($publication, $loggedUser){
	if(!$loggedUser->user_id){throw new Exception("Permission denied",501);}
	$publication['user_id'] = $loggedUser->user_id;
	$publication['date'] = date("Y-m-d H:i:s");
	$GLOBALS['DB']->insert('publications',(array)$publication);
	$pub_id = $GLOBALS['DB']->id();
	return getPublication($pub_id);
}

function updatePublication($publication, $loggedUser){
	$is_admin = check_admin($loggedUser);
	if(!$is_admin && (!$loggedUser->user_id || $loggedUser->user_id!= $publication['user_id'])){throw new Exception("Permission denied",501);}
	$publication['date'] = date("Y-m-d H:i:s"); 	$GLOBALS['DB']->update('publications',array('title'=>$publication['title'],'authors'=>$publication['authors'],'journal'=>$publication['journal'],'volume'=>$publication['volume'],'page'=>$publication['page'],'year'=>$publication['year'],'doi'=>$publication['doi'],'pmid'=>$publication['pmid'],'url'=>$publication['url'],'abstract'=>$publication['abstract'],'date'=>$publication['date']), array('publication_id'=>$publication['publication_id']));
	return getPublication($publication['publication_id']);
}

function deletePublication($publication_id, $loggedUser){
	$loggedUserId = $loggedUser->user_id;
	$publiUserId = $GLOBALS['DB']->get("publications","user_id",array('publication_id'=>$publication_id));
	$is_admin = check_admin($loggedUser);
	if(!$is_admin && (!$loggedUserId || $loggedUserId!= $publiUserId)){throw new Exception("Permission denied",501);}
	$GLOBALS['DB']->delete('publications',array("publication_id" => $publication_id));
	return $publication_id;
}

function getPubmed($pmid){
	$xml = file_get_contents('http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?format=xml&db=pubmed&id='.$pmid);
	if($xml){
		$output = new SimpleXMLElement($xml);
		if(isset($output->PubmedArticle->MedlineCitation->Article->ArticleTitle)){
			$date = (isset($output->PubmedArticle->MedlineCitation->Article->ArticleDate->Year)) ? $output->PubmedArticle->MedlineCitation->Article->ArticleDate->Year : ((isset($output->PubmedArticle->MedlineCitation->Article->Journal->JournalIssue->PubDate->Year)) ? $output->PubmedArticle->MedlineCitation->Article->Journal->JournalIssue->PubDate->Year : ((isset($output->PubmedArticle->MedlineCitation->DateCreated->Year)) ? $output->PubmedArticle->MedlineCitation->DateCreated->Year : ""));
			$date = (string) $date;
			$title = (string) $output->PubmedArticle->MedlineCitation->Article->ArticleTitle;
			$volume = (string) $output->PubmedArticle->MedlineCitation->Article->Journal->JournalIssue->Volume;
			if($output->PubmedArticle->MedlineCitation->Article->Journal->JournalIssue->Issue){
				$volume .= ":".$output->PubmedArticle->MedlineCitation->Article->Journal->JournalIssue->Issue;
			}
			$page = (string) $output->PubmedArticle->MedlineCitation->Article->Pagination->MedlinePgn;
			$authors = array();
			if($output->PubmedArticle->MedlineCitation->Article->AuthorList->Author){
				foreach($output->PubmedArticle->MedlineCitation->Article->AuthorList->Author as $author){
					$authors[] = (string) $author->ForeName." ".$author->LastName;
				}
			}
			$ids = $output->PubmedArticle->MedlineCitation->Article->ELocationID;
			foreach($ids as $id){
				if((string)$id->attributes()->EIdType =='doi' && (string)$id->attributes()->ValidYN == 'Y'){ $doi = (string)$id; }
			}
			$abstract = (string)$output->PubmedArticle->MedlineCitation->Article->Abstract->AbstractText;
			$journal = (string) $output->PubmedArticle->MedlineCitation->Article->Journal->Title;
			$pubmed = array("pmid" => $pmid,"journal" => $journal,"title" => $title,"authors" => implode(", ",$authors),"year" => +$date,"volume"=>$volume,"page"=>$page,"doi"=>$doi,"abstract"=>$abstract);
			return $pubmed;
		}
	}
}

?>