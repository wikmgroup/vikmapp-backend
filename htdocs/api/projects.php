<?php


/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/**
 * listProjects
 *
 * lists all projects for a given user
 *
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function listProjects($loggedUser){

	$projects = $GLOBALS['DB']->select(
		"projects",
		array(
			"[><]project_groups" => "project_id",
			"[><]user_groups" => array("project_groups.group_id" => "group_id"),
			"[><]users" => array("user_groups.user_id" => "user_id")
		),
		array(
			"projects.project_id (project_id)",
			"projects.name (name)",
			"projects.user_id (user_id)",
			"users.firstname (firstname)",
			"users.lastname (lastname)"
		),
		array(
			"users.user_id" => $loggedUser->user_id
		)

	);
	$return = array();
	foreach($projects as $project){
		$return[$project['project_id']] = $project;
	}
	return array_values($return);
}

/**
 * getProject
 *
 * Gets details about a project
 *
 * @param int $project_id
 * @param stdClass $loggedUser * @return array
 * @author Robin
 */

function getProject($project_id,$loggedUser){
	$projects = $GLOBALS['DB']->select(
		"projects",
		array(
			"[><]project_groups" => "project_id",
			"[><]user_groups" => array("project_groups.group_id" => "group_id"),
			"[><]users" => array("user_groups.user_id" => "user_id")
		),
		array(
			"projects.project_id (project_id)",
			"projects.name (name)",
			"projects.description (description)",
			"projects.user_id (user_id)",
			"users.firstname (firstname)",
			"users.lastname (lastname)"
		),
		array(
			"AND" => array(
				"users.user_id" => $loggedUser->user_id,
				"projects.project_id" => $project_id
			)
		)
	);
	if(count($projects)){
		$project = $projects[0];
		$project['groups'] = $GLOBALS['DB']->select(
			"groups",
			array(
				"[><]users" => array("leader_id" => "user_id"),
				"[><]project_groups" => "group_id"
			),
			array(
				"groups.name (name)",
				"groups.group_id (group_id)",
				"users.firstname (firstname)",
				"users.lastname (lastname)",
				"groups.leader_id (leader_id)"
			),
			array(
				"project_groups.project_id" => $project_id
			)
		);
		return $project;
	}
	throw new Exception("ERROR: unknown project or permission denied", 501);



}

?>