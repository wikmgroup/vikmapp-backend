<?php
// The email address of the ViKM administrator
if(!defined("CONTACT_EMAIL")) define("CONTACT_EMAIL","first.last@email.com");

// The name of the ViKM instance
if(!defined("SITE_TITLE")) define("SITE_TITLE","ViKM APP");

// Path to the data directory. All datasets will be stored in this directory.
// This directory must be writable by apache user.
if(!defined('DATA_PATH')) define('DATA_PATH',__DIR__."/../data");
// This directory must be writable by apache user.
if(!defined('TMP_PATH')) define('TMP_PATH',"/tmp");

// The type of RDBMS to use
if(!defined("DBTYPE")) define("DBTYPE","sqlite");

// For MySQL connection
// if(!defined("DBBASE")) define("DBBASE","mysql_database");
// if(!defined("DBSERVER")) define("DBSERVER","mysql_server");
// if(!defined("DBNAME")) define("DBNAME","mysql_username");
// if(!defined("DBPWD")) define("DBPWD","mysql_user_password");

// For Sqlite3 connection. Path to the DB file
if(!defined("DBFILE")) define("DBFILE",DATA_PATH."/database.sqlite");

// Path to the tools directory
if(!defined('INCLUDE_PATH')) define("INCLUDE_PATH",dirname(__FILE__)."/../tools/");

// Whether to server should accept Cross-Origin request or not. Should be set to false in production.
if(!defined('CORS')) define('CORS',true);

// Set the debug state of the application. Might be used to display some debugging messages
if(!defined('DEBUG')) define('DEBUG',true);

if(!defined('GRIP_PATH')) define('GRIP_PATH',"/usr/local/bin/grip");
if(!defined('PANDOC_PATH')) define('PANDOC_PATH',"/usr/local/bin/pandoc");

// Set secret key for JWT: MUST BE CUSTOMIZED //
if(!defined('JWT_SECRET')) define('JWT_SECRET','rE|Fa1%Is5Q30p+XO8');


// two factor authentication using google authenticator // 
if(!defined('GA_ENABLED')) define("GA_ENABLED",true);

?>
